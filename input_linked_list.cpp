#include <bits/stdc++.h>
using namespace std;
class Node
{
public:
    int val;
    Node *next;
    Node(int val)
    {
        this->val = val;
        this->next = NULL;
    }
};

void insert_at_tail(Node *&head, int v)
{
    Node *newNode = new Node(v); // create a Node with constructor
    if (head == NULL)
    {
        head = newNode;
        return;
    }
    // insert the node
    Node *tmp = head;
    while (tmp->next != NULL)
    {
        tmp = tmp->next;
    }                    // tmp at last node
    tmp->next = newNode; // tmp->next means for moving dereference from tmp to next
}

void print_linked_list(Node *head)
{
    cout << endl;
    cout << "Your Linked List: ";
    Node *tmp = head;
    while (tmp != NULL)
    {
        cout << tmp->val << " ";
        tmp = tmp->next;
    }
    cout << endl
         << endl;
}

int main()
{
    Node *head = NULL;
    while (true)
    {
        int v;
        cin>>v;
        if(v==-1) break;
        insert_at_tail(head,v);
    }
    print_linked_list(head);
    return 0;
}